// CRUD Operations

// Insert Documents (CREATE)
/*
	Syntax:
		Insert one document
			-db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

// Insert Many Documents
/*
	- db.collectionName.insertMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
	])
	])

*/

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}

])

db.courses.insertMany([
		{
			"name": "Javascript 101",
            "price": 5000,
            "description": "Introduction to Javascript",
            "isActive": true
		},

		{
			"name": "HTML 101",
            "price": 2000,
            "description": "Introduction to HTML",
            "isActive": true
		},
		{
			"name": "CSS 101",
            "price": 2500,
            "description": "Introduction to CSS",
            "isActive": false
		}


	])

// Find Documents (READ)

/*
	Syntax:
		db.collectionName.find() - this will retrieve all documents

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria

		db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that will match our criteria

		db.collection.findOne({}) - will return the first document in the collection


*/

db.users.find();

db.users.find({
	"firstName": "Jane"
})

db.users.find({
	"lastName": "Armstrong",
	"age": 82
})

// Updating documents (UPDATE)

/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
					"fieldToBeUpdated": "updatedValue"
					}
			}
		})


		db.collectionName.updateMany({
		{
			"criteria": "value",
		},
		{
			$set: {
					"fieldToBeUpdated": "updatedValue"
					}
		}



		})


*/

db.users.insertOne({
			"firstName": "Test",
			"lastName": "Test",
			"age": 0,
			"email": "test@mail.com",
			"department": "none"
})

// Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}


)

// Removing a field

db.users.updateOne(
{
	"firstName": "Bill"
},
{
	$unset: {
		"status": "active"
	}
})

// Updating Multiple Documents
db.users.updateMany(
	{	
		"department": "none"
	},
	{$set: {
			"department": "HR"
			
		}}

)
// Renaming a field
db.users.updateMany(
	{	
		"department": "none",
		"department": "Operations"
	},
	{$rename: {
			"department": "dept"
			
		}}
)

/*
	Mini Activity:
		Courses collection, update the HTML 101 course - make isActive to false

		add enrollees field to all the documents in our courses collection - enrollees 10

		"name": "HTML 101",
            "price": 2000,
            "description": "Introduction to HTML",
            "isActive": true

*/

db.courses.updateOne(
{
	"name": "HTML 101"
},
	{$set:{
		"isActive": false
	}}


)

db.courses.updateMany({},
	{$set:{
		"Enrolles": 10
	}}

)

//Deleting Documents (DELETE)

db.users.insertOne({
	"firstName": "Test"
		
})

// Deleting a single document

/*
	Syntax:
		db.collectionName.deleteOne({"criteria": "value"})

*/

db.users.deleteOne({
	"firstName": "Test"
})

// Deleting multiple documents

/*
	Syntax:
		db.collectionName.deleteMany({"criteria": "value"})
		to delete all documents:
		db.collectionName.deleteMany({})
*/

db.users.deleteMany({
	"dept": "none"
})

// 